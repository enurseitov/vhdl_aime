library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sequencer is
	generic(ADDR_WIDTH : integer := 5);
	port(
		clk                                : in  std_logic;
		rst_n                              : in  std_logic;
		en                                 : in  std_logic;
		addr_read, addr_write              : out STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0);
		dataval                            : out std_logic;
		ram1_cs, ram2_cs, ram1_wr, ram2_wr : out std_logic;
		acc_en                             : out std_logic; -- signal to control accumulator
		acc_rst_n                          : out std_logic
	);
end entity sequencer;

--architecture RTL of sequencer is
--	type state_type is (idle, ram_fill, delay1, calc, delay2, wait_next);
--	SIGNAL state : state_type;
--	signal count : unsigned(ADDR_WIDTH - 1 downto 0) := (others => '0');
--begin
--	--	COMB_PROC : process(current_state, ff1) is
--	--	begin
--	--	end process COMB_PROC;
--
--	SYNC_PROC : process(clk, rst_n) is
--	begin
--		if rst_n = '0' then
--			state <= idle;
--		elsif rising_edge(clk) then
--			case state is
--				when idle =>
--					if en = '1' then
--						state   <= ram_fill;
--						ram1_cs <= '1';
--						ram1_wr <= '1';
--						
--					else 
--						ram1_wr <= '0';
--					end if;
--						
--					dataval <= '0';
--					acc_en <= '0';
--
--				when ram_fill =>
--					if count = 31 then
--						state <= delay1;
--
--						ram1_wr <= '0';
--					end if;
--
--					count <= count + 1;
--
--				when delay1 =>
--					if count = 1 then
--						state     <= calc;
--						acc_en    <= '1';
--						acc_rst_n <= '1';
--					end if;
--					count <= count + 1;
--
--				when calc =>
--					if count = 31 then
--						state <= delay2;
--
--					end if;
--
--					count <= count + 1;
--
--				when delay2 =>
--					if count = 1 then
--						state  <= wait_next;
--						acc_en <= '0';
--						dataval  <= '1';
--						count  <= (others => '0');
--
--					else
--						count <= count + 1;
--					end if;
--				when wait_next =>
--					if en = '1' then
--						state     <= ram_fill;
--						dataval   <= '0';
--						ram1_cs   <= '1';
--						ram1_wr   <= '1';
--						acc_rst_n <= '0';
--					end if;
--
--			end case;
--		end if;
--	end process SYNC_PROC;
--
--	addr_read  <= std_logic_vector(count);
--	addr_write <= std_logic_vector(count);
--
--	ram2_cs <= '0';
--	ram2_wr <= '0';
--
--end architecture RTL;
architecture RTL of sequencer is
	type state_type is (idle, ram_fill, delay1, calc, delay2, wait_next);
	SIGNAL state : state_type;
	signal count : unsigned(ADDR_WIDTH - 1 downto 0) := (others => '0');
begin
	--	COMB_PROC : process(current_state, ff1) is
	--	begin
	--	end process COMB_PROC;

	SYNC_PROC : process(clk) is
	begin
		if rising_edge(clk) then
			if rst_n = '0' then
				state <= idle;

			else
				case state is
					when idle =>
						if en = '1' then
							state   <= ram_fill;
							ram1_cs <= '1';
							ram1_wr <= '1';

						else
							ram1_wr <= '0';
						end if;

						dataval <= '0';
						acc_en  <= '0';

					when ram_fill =>
						if count = 31 then
							state <= delay1;

							ram1_wr <= '0';
						end if;

						count <= count + 1;

					when delay1 =>
						if count = 1 then
							state     <= calc;
							acc_en    <= '1';
							acc_rst_n <= '1';
						end if;
						count <= count + 1;

					when calc =>
						if count = 31 then
							state <= delay2;

						end if;

						count <= count + 1;

					when delay2 =>
						if count = 1 then
							state   <= wait_next;
							acc_en  <= '0';
							dataval <= '1';
							count   <= (others => '0');

						else
							count <= count + 1;
						end if;
					when wait_next =>
						if en = '1' then
							state     <= ram_fill;
							dataval   <= '0';
							ram1_cs   <= '1';
							ram1_wr   <= '1';
							acc_rst_n <= '0';
						end if;

				end case;
			end if;
		end if;
	end process SYNC_PROC;
	addr_read  <= std_logic_vector(count);
	addr_write <= std_logic_vector(count);

	ram2_cs <= '0';
	ram2_wr <= '0';

end architecture RTL;