library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TOP is
	generic(DATA_WIDTH     : integer := 12;
		    ADDR_WIDTH     : integer := 5;
		    ROM_DATA_WIDTH : integer := 11;
		    ROM_ADDR_WIDTH : integer := 4
	);

	Port(clk      : in  STD_LOGIC;
		 rst_n    : in  STD_LOGIC;
		 enable   : in  STD_LOGIC;
		 datain_i : in  STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
		 dataout  : out STD_LOGIC_VECTOR(DATA_WIDTH + ROM_DATA_WIDTH + ADDR_WIDTH - 1 downto 0); --????
		 dataval  : out STD_LOGIC);
end TOP;

architecture Behavioral of TOP is
	component datain_delay
		generic(DATA_WIDTH : integer);
		port(
			clk   : in  std_logic;
			rst_n : in  std_logic;
			i     : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			o     : out std_logic_vector(DATA_WIDTH - 1 downto 0)
		);
	end component datain_delay;

	--	component sequencer
	--		
	--				port(
	--			en, clk, rst_n : in  STD_LOGIC;
	--			addr           : out STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0);
	--			mode           : out STD_LOGIC;
	--			data_val       : out STD_LOGIC
	--		);
	--	end component sequencer;

	component sequencer
		generic(ADDR_WIDTH : integer);
		port(
			clk                                : in  std_logic;
			rst_n                              : in  std_logic;
			en                                 : in  std_logic;
			addr_read, addr_write              : out STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0);
			dataval                            : out std_logic;
			ram1_cs, ram2_cs, ram1_wr, ram2_wr : out std_logic;
			acc_en                             : out std_logic;
			acc_rst_n                          : out std_logic
		);
	end component sequencer;

	component dpram
		generic(
			DATA_WIDTH : integer;
			ADDR_WIDTH : integer
		);
		port(
			addr1, addr2         : in  STD_LOGIC_VECTOR(ADDR_WIDTH - 1 downto 0);
			cs1, cs2             : in  STD_LOGIC;
			wr1, wr2             : in  STD_LOGIC;
			data_in1, data_in2   : in  STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
			data_out1, data_out2 : out STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);
			clk1, clk2           : in  STD_LOGIC
		);
	end component dpram;

	component rom
		generic(
			ROM_DATA_WIDTH : integer;
			ROM_ADDR_WIDTH : integer
		);
		port(
			clk      : in  std_logic;
			addr_in  : in  std_logic_vector((ROM_ADDR_WIDTH - 1) downto 0);
			data_out : out std_logic_vector((ROM_DATA_WIDTH - 1) downto 0)
		);
	end component rom;

--	component multiplier
--		generic(
--			DATA_WIDTH     : integer;			
--			ROM_DATA_WIDTH : integer		
--		);
--		port(
--			clk, rst_n : in  std_logic;
--			data_in_A  : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
--			data_in_B  : in  std_logic_vector(ROM_DATA_WIDTH - 1 downto 0);
--			data_out   : out std_logic_vector((DATA_WIDTH + ROM_DATA_WIDTH) - 1 downto 0)
--		);
--	end component multiplier;
	
	COMPONENT IP_multiplier
  PORT (
    clk : IN STD_LOGIC;
    a : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    b : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
    p : OUT STD_LOGIC_VECTOR(22 DOWNTO 0)
  );
END COMPONENT;

	component accumulator
		generic(
			DATA_WIDTH     : integer;
			ADDR_WIDTH     : integer;
			ROM_DATA_WIDTH : integer;
			ROM_ADDR_WIDTH : integer
		);
		port(
			clk                  : in  STD_LOGIC;
			rst_n                : in  STD_LOGIC;
			data_in              : in  STD_LOGIC_VECTOR(DATA_WIDTH + ROM_DATA_WIDTH - 1 downto 0);
			mode_in              : in  STD_LOGIC;
			data_out             : out STD_LOGIC_VECTOR(DATA_WIDTH + ROM_DATA_WIDTH + ADDR_WIDTH - 1 downto 0);
			rst_n_from_sequencer : in  std_logic
		--data_val : out STD_LOGIC
		);
	end component accumulator;

	signal addr_internal                      : std_logic_vector(ADDR_WIDTH - 1 downto 0);
	signal dpram_data_internal                : std_logic_vector(DATA_WIDTH - 1 downto 0);
	signal rom_data_internal                  : std_logic_vector(ROM_DATA_WIDTH - 1 downto 0);
	signal mult_output                        : std_logic_vector((DATA_WIDTH + ROM_DATA_WIDTH) - 1 downto 0);
	--signal mode : std_logic;
	signal datain_delayed                     : std_logic_vector(DATA_WIDTH - 1 downto 0);
	signal ram1_cs, ram2_cs, ram1_wr, ram2_wr : std_logic;
	signal acc_en, acc_rst_n                  : std_logic;

begin
	DEL : component datain_delay
		generic map(
			DATA_WIDTH => DATA_WIDTH
		)
		port map(
			clk   => clk,
			rst_n => rst_n,
			i     => datain_i,
			o     => datain_delayed
		);

	--SEQ:sequencer
	--	port map(
	--		en    => enable,
	--		clk   => clk,
	--		rst_n => rst_n,
	--		addr  => addr_internal,
	--		mode  => mode,
	--		data_val => dataval
	--		
	--	); 

	SEQ : entity work.sequencer
		generic map(
			ADDR_WIDTH => ADDR_WIDTH
		)
		port map(
			clk       => clk,
			rst_n     => rst_n,
			en        => enable,
			addr_read => addr_internal,
			--addr_write => addr_write,
			dataval   => dataval,
			ram1_cs   => ram1_cs,
			ram2_cs   => ram2_cs,
			ram1_wr   => ram1_wr,
			ram2_wr   => ram2_wr,
			acc_en    => acc_en,
			acc_rst_n  => acc_rst_n
		);

	DPRM : dpram
		generic map(
			DATA_WIDTH => DATA_WIDTH,
			ADDR_WIDTH => ADDR_WIDTH
		)
		port map(
			addr1     => addr_internal,
			addr2     => (addr_internal'range => '0'),
			cs1       => ram1_cs,
			cs2       => ram2_cs,
			wr1       => ram1_wr,
			wr2       => ram2_wr,
			data_in1  => datain_delayed,
			data_in2  => (datain_i'range => '0'),
			data_out1 => dpram_data_internal,
			--	data_out2 => (others => '0'),
			clk1      => clk,
			clk2      => clk
		);

	ROMM : rom
		generic map(
			ROM_DATA_WIDTH => ROM_DATA_WIDTH,
			ROM_ADDR_WIDTH => ROM_ADDR_WIDTH
		)
		port map(
			clk      => clk,
			addr_in  => addr_internal(3 downto 0),
			data_out => rom_data_internal
		);

--	MUL : entity work.multiplier(sync)
--	--MUL : entity work.multiplier(sequental)
--		generic map(
--			DATA_WIDTH     => DATA_WIDTH,
--			ROM_DATA_WIDTH => ROM_DATA_WIDTH		
--		)
--		port map(
--			clk       => clk,
--			rst_n     => rst_n,
--			data_in_A => dpram_data_internal,
--			data_in_B => rom_data_internal,
--			data_out  => mult_output
--		);
		
		MUL_IP : IP_multiplier
  PORT MAP (
    clk => clk,
    a => dpram_data_internal,
    b => rom_data_internal,
    p => mult_output
  );

	ACCU : accumulator
		generic map(
			DATA_WIDTH     => DATA_WIDTH,
			ADDR_WIDTH     => ADDR_WIDTH,
			ROM_DATA_WIDTH => ROM_DATA_WIDTH,
			ROM_ADDR_WIDTH => ROM_ADDR_WIDTH
		)
		port map(
			clk      => clk,
			rst_n    => rst_n,
			data_in  => mult_output,
			mode_in  => acc_en,
			data_out => dataout,
			rst_n_from_sequencer  => acc_rst_n
		--data_val => dataval
		

		);

end Behavioral;

