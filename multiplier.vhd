library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity multiplier is


generic (DATA_WIDTH : integer;
		--ADDR_WIDTH : integer;
		ROM_DATA_WIDTH : integer
		--ROM_ADDR_WIDTH : integer
		);
		
--generic (DATA_WIDTH : integer := 12;
--		ADDR_WIDTH : integer := 5;
--		ROM_DATA_WIDTH : integer :=11;
--		ROM_ADDR_WIDTH : integer :=4
--		);

  port (
  	clk, rst_n : in std_logic;
	data_in_A : in std_logic_vector (DATA_WIDTH-1 downto 0);
	data_in_B : in std_logic_vector (ROM_DATA_WIDTH-1 downto 0);
	data_out : out std_logic_vector ((DATA_WIDTH + ROM_DATA_WIDTH) - 1 downto 0)

  ) ;
end entity ; -- multiplie

architecture sync of multiplier is
--signal ff_A : std_logic_vector (DATA_WIDTH-1 downto 0);
--signal ff_B : std_logic_vector (ROM_DATA_WIDTH-1 downto 0);
--signal FF_res: std_logic_vector ((DATA_WIDTH + ROM_DATA_WIDTH) - 1 downto 0);
begin
process( clk, rst_n )
begin
	if rst_n = '0' then
		data_out <= (others => '0');
	elsif clk'event and clk = '1' then
--	ff_A <= data_in_A;
--	ff_B <= data_in_B;
--	data_out <= FF_res;
--	FF_res <= std_logic_vector(unsigned(ff_A)*unsigned(ff_B));	
	
	data_out <= std_logic_vector(unsigned(data_in_A)*unsigned(data_in_B));			
		
	end if ;		
end process ; -- 

end architecture sync; -- arch

architecture sequental of multiplier is
signal Y1, Y2, Y3 : unsigned (14 downto 0) := (others => '0') ;
constant Z : unsigned(7 downto 0) := (others => '0');

begin


process( clk, rst_n )
begin
	if rst_n = '0' then
		data_out <= (others => '0');
	elsif clk'event and clk = '1' then
		Y1  <=  unsigned(data_in_B)  *  unsigned(data_in_A( 3 downto  0)) ;
      Y2  <=  unsigned(data_in_B)  *  unsigned(data_in_A(7 downto  4)) ;
		Y3  <=  unsigned(data_in_B)  *  unsigned(data_in_A(11 downto  8)) ;
		
		data_out <=std_logic_vector(
          (Z(7 downto 0)  &  Y1 )  + 
			 (Z(7 downto 4)  &  Y2  &  Z( 3 downto 0))  +
										(Y3  &  Z( 7 downto  0))
										);
		
	end if ;		
end process ; -- 

end architecture sequental; 